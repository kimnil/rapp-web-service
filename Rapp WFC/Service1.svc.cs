﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Rapp_WFC
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public DataSet ExecuteQuery(string sql) 
        {
            DataSet dataSet = new DataSet();

            try
            {
                SqlConnection objSQL = new SqlConnection("Data Source=localhost;Initial Catalog=rappdb;User Id=g09_adm;Password=vhY$VJX7;");
                SqlDataAdapter da = new SqlDataAdapter(sql, objSQL);
                da.Fill(dataSet);
            }
            catch
            {
                Console.WriteLine("Caught exception in ExecuteQuery()");
            }

            //return "hej jag är en web service";
            return dataSet;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
